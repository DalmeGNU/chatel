#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>

#define VERSION "0.1"

#define CHATEL_NICKNAME_SIZE 32
#define CHATEL_MESSAGE_SIZE 256
#define CHATEL_DATE_SIZE 32
#define CHATEL_BUFFER_SIZE 324

#define CHATEL_MILLISLEEP 1000000

#define LOG_FILE "/home/dalme/chatel/roomlog"

static int get_file_size(char *filename)
{
	struct stat st;
	stat(filename, &st);
	return st.st_size;
}

void* read_chat()
{
	int fd;
	int min_pos = get_file_size(LOG_FILE)-1;
	if (min_pos < 0)
		min_pos = 0;

	printf("Joining chat room... DONE\n");
	for (;;) {
		int size = get_file_size(LOG_FILE);
		if (!size) {
			usleep(CHATEL_MILLISLEEP);
			continue;
		}
		fd = open(LOG_FILE, O_RDONLY);
		char *fc = calloc(1, size);
		read(fd, fc, size);
		/* update chat with new messages */
		printf("\33[2K\r%s", &fc[min_pos]);
		//printf("%s", fc);
		close(fd);
		min_pos = size;
		usleep(CHATEL_MILLISLEEP);
		//puts("\x1B[2J");
	}
	return NULL;
}

void* write_chat(void *nickname)
{
	int fd;
	int exit = 0;
	char *message;
	while (!exit) {
		message = calloc(1, CHATEL_MESSAGE_SIZE);
		printf("%s: ", (char *)nickname);
		fgets(message, CHATEL_MESSAGE_SIZE, stdin);
		if (!strcmp(message, "/quit\n"))
			exit = 1;
		else {
			time_t t = time(NULL);
			struct tm tm = *localtime(&t);
			char *date_info = calloc(1, CHATEL_DATE_SIZE);
			snprintf(date_info, CHATEL_DATE_SIZE, "%d-%d-%d %d:%d:%d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
			char *buffer = calloc(1, CHATEL_BUFFER_SIZE);
			snprintf(buffer, CHATEL_BUFFER_SIZE, "%s %s: %s", date_info, (char *)nickname, message);
			fd = open(LOG_FILE, O_WRONLY | O_APPEND);
			write(fd, buffer, strlen(buffer));
			close(fd);
			free(date_info);
			free(buffer);
		}
		free(message);
	}
	return NULL;
}

int main(int argc, char **argv)
{
	char *nickname = calloc(1, CHATEL_NICKNAME_SIZE);
	printf("\nCHATEL v%s: CHAt under TELnet\n\n", VERSION);
	while (!(*nickname)) {
		printf("Nickname: ");
		fgets(nickname, CHATEL_NICKNAME_SIZE, stdin);
		if (nickname[0] == '\n')
			nickname[0] = 0;
		else
			nickname[strlen(nickname)-1] = 0;
		printf("\n");
	}
	pthread_t read_thread;
	pthread_t write_thread;
	pthread_create(&read_thread, NULL, read_chat, NULL);
	pthread_create(&write_thread, NULL, write_chat, nickname);
	pthread_join(write_thread, NULL);
	free(nickname);
	return 0;
}
